﻿using EFCoreRecursive.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleAppEFCoreRecursive.Data
{
    public class Process
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public string Title { get; set; }

        public Process Parent { get; set; }

        public int? ParentId { get; set; }

        public List<Process> Child { get; set; }

        public List<Document>? Documents { get; set; }

    }
}
