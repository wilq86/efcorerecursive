﻿using ConsoleAppEFCoreRecursive.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EFCoreRecursive.Data
{
    public class Document
    {
        public int Id { get; set; } 

        public string Name { get; set; }

        public int ProcessId { get; set; }

        public Process? Process { get; set; }    
    }
}
