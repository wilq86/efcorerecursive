﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace EFCoreRecursive.Migrations
{
    /// <inheritdoc />
    public partial class UpperCaseInProcess : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_processes_processes_ParentId",
                table: "processes");

            migrationBuilder.DropPrimaryKey(
                name: "PK_processes",
                table: "processes");

            migrationBuilder.RenameTable(
                name: "processes",
                newName: "Processes");

            migrationBuilder.RenameIndex(
                name: "IX_processes_ParentId",
                table: "Processes",
                newName: "IX_Processes_ParentId");

            migrationBuilder.AddPrimaryKey(
                name: "PK_Processes",
                table: "Processes",
                column: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_Processes_Processes_ParentId",
                table: "Processes",
                column: "ParentId",
                principalTable: "Processes",
                principalColumn: "Id");
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Processes_Processes_ParentId",
                table: "Processes");

            migrationBuilder.DropPrimaryKey(
                name: "PK_Processes",
                table: "Processes");

            migrationBuilder.RenameTable(
                name: "Processes",
                newName: "processes");

            migrationBuilder.RenameIndex(
                name: "IX_Processes_ParentId",
                table: "processes",
                newName: "IX_processes_ParentId");

            migrationBuilder.AddPrimaryKey(
                name: "PK_processes",
                table: "processes",
                column: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_processes_processes_ParentId",
                table: "processes",
                column: "ParentId",
                principalTable: "processes",
                principalColumn: "Id");
        }
    }
}
