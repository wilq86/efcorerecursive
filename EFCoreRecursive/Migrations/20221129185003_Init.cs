﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

#pragma warning disable CA1814 // Prefer jagged arrays over multidimensional

namespace EFCoreRecursive.Migrations
{
    /// <inheritdoc />
    public partial class Init : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "processes",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    Title = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    ParentId = table.Column<int>(type: "int", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_processes", x => x.Id);
                    table.ForeignKey(
                        name: "FK_processes_processes_ParentId",
                        column: x => x.ParentId,
                        principalTable: "processes",
                        principalColumn: "Id");
                });

            migrationBuilder.InsertData(
                table: "processes",
                columns: new[] { "Id", "Name", "ParentId", "Title" },
                values: new object[,]
                {
                    { 1, "Charles Montgomery Burns", null, "Owner" },
                    { 2, "Waylon Smithers, Jr.", 1, "Assistant" },
                    { 3, "Lenny Leonard", 2, "Technical Supervisor" },
                    { 4, "Carl Carlson", 2, "Safety Operations Supervisor" },
                    { 5, "Inanimate Carbon Rod", 4, "Rod" },
                    { 6, "Homer Simpson", 5, "Safety Inspector" }
                });

            migrationBuilder.CreateIndex(
                name: "IX_processes_ParentId",
                table: "processes",
                column: "ParentId");
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "processes");
        }
    }
}
