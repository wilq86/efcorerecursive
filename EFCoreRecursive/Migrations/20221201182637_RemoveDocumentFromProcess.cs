﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace EFCoreRecursive.Migrations
{
    /// <inheritdoc />
    public partial class RemoveDocumentFromProcess : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Processes_Documents_DocumentId",
                table: "Processes");

            migrationBuilder.DropIndex(
                name: "IX_Processes_DocumentId",
                table: "Processes");

            migrationBuilder.DropColumn(
                name: "DocumentId",
                table: "Processes");
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "DocumentId",
                table: "Processes",
                type: "int",
                nullable: true);

            migrationBuilder.UpdateData(
                table: "Processes",
                keyColumn: "Id",
                keyValue: 1,
                column: "DocumentId",
                value: null);

            migrationBuilder.UpdateData(
                table: "Processes",
                keyColumn: "Id",
                keyValue: 2,
                column: "DocumentId",
                value: null);

            migrationBuilder.UpdateData(
                table: "Processes",
                keyColumn: "Id",
                keyValue: 3,
                column: "DocumentId",
                value: null);

            migrationBuilder.UpdateData(
                table: "Processes",
                keyColumn: "Id",
                keyValue: 4,
                column: "DocumentId",
                value: null);

            migrationBuilder.UpdateData(
                table: "Processes",
                keyColumn: "Id",
                keyValue: 5,
                column: "DocumentId",
                value: null);

            migrationBuilder.UpdateData(
                table: "Processes",
                keyColumn: "Id",
                keyValue: 6,
                column: "DocumentId",
                value: null);

            migrationBuilder.CreateIndex(
                name: "IX_Processes_DocumentId",
                table: "Processes",
                column: "DocumentId");

            migrationBuilder.AddForeignKey(
                name: "FK_Processes_Documents_DocumentId",
                table: "Processes",
                column: "DocumentId",
                principalTable: "Documents",
                principalColumn: "Id");
        }
    }
}
