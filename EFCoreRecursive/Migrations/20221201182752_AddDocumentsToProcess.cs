﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace EFCoreRecursive.Migrations
{
    /// <inheritdoc />
    public partial class AddDocumentsToProcess : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "ProcessId",
                table: "Documents",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.CreateIndex(
                name: "IX_Documents_ProcessId",
                table: "Documents",
                column: "ProcessId");

            migrationBuilder.AddForeignKey(
                name: "FK_Documents_Processes_ProcessId",
                table: "Documents",
                column: "ProcessId",
                principalTable: "Processes",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Documents_Processes_ProcessId",
                table: "Documents");

            migrationBuilder.DropIndex(
                name: "IX_Documents_ProcessId",
                table: "Documents");

            migrationBuilder.DropColumn(
                name: "ProcessId",
                table: "Documents");
        }
    }
}
