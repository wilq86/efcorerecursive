﻿using ConsoleAppEFCoreRecursive.Data;
using EFCoreRecursive.Data;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Diagnostics.Metrics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleAppEFCoreRecursive
{
    public class AppDbConxtext : DbContext
    {
        public DbSet<Process> Processes { get; set; }

        public DbSet<Document> Documents { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Process>()
                .HasData(new List<Process>()
                {
            // org chart
            // https://simpsons.fandom.com/wiki/Springfield_Nuclear_Power_Plant
            new() {Id = 1, Name = "Charles Montgomery Burns", Title = "Owner"},
            new() {Id = 2, Name = "Waylon Smithers, Jr.", Title = "Assistant", ParentId = 1},
            new() {Id = 3, Name = "Lenny Leonard", Title = "Technical Supervisor", ParentId = 2},
            new() {Id = 4, Name = "Carl Carlson", Title = "Safety Operations Supervisor", ParentId = 2},
            new() {Id = 5, Name = "Inanimate Carbon Rod", Title = "Rod", ParentId = 4},
            new() {Id = 6, Name = "Homer Simpson", Title = "Safety Inspector", ParentId = 5}
                });

        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
                optionsBuilder.UseSqlServer(@"Server=localhost\SQLEXPRESS;Database=Recursive;Trusted_Connection=True;MultipleActiveResultSets=True;TrustServerCertificate=True;");
            }
        }

        public async Task<List<Process>> ProcessWithChildsAndParents(int id)
        {
            var query = Processes.FromSqlRaw(
                @"	WITH Rec (id, name, title, parentid, below) AS (
                    SELECT id, name, title, parentid, 0 
                    FROM dbo.Processes    
                    WHERE processes.Id = 1         
                    UNION ALL
                    SELECT e.id, e.name, e.title, e.parentid, r.below + 1
                    FROM dbo.Processes e    
                    INNER JOIN Rec r 
                        ON r.Id = e.ParentId
                )
                SELECT * FROM Rec", id);

            return await query
                   //.AsNoTrackingWithIdentityResolution()
                   .ToListAsync();
        }

        public async Task<List<Process>> GetTree(int id)
        {
            var items = await ProcessWithChildsAndParents(id);

            var ids = items.Select(x => x.Id).ToList();
            
            Documents.Where(x => ids.Contains(x.ProcessId)).Load();

            return await ProcessWithChildsAndParents(id);
        }

        public List<Process> GetTreeSimple(List<Process> processesToReturn)
        {

            foreach (var item in processesToReturn)
            {
                item.Child = new List<Process>();

                var children =  Processes.Where(x => x.ParentId == item.Id)?.ToList();

                if (children != null && children.Count > 0)
                {
                    item.Child.AddRange(children);
                }
            }

            processesToReturn = processesToReturn.Where(x => x.ParentId == null).ToList();

            return processesToReturn;
        }
    }
}
