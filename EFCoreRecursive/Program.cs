﻿// See https://aka.ms/new-console-template for more information
using ConsoleAppEFCoreRecursive;
using ConsoleAppEFCoreRecursive.Data;
using Microsoft.EntityFrameworkCore;
using static System.Console;

WriteLine("Hello, World!");

AppDbConxtext context = new AppDbConxtext();

var list = await context.GetTree(1);

foreach (var item in list)
{
    WriteLine($"Employe {item.Id}");

    Write("Parent: ");
    print(item, 0);
    WriteLine();

    WriteLine($"Childs: {string.Join(",", item?.Child?.Select(x => x.Id) ?? new List<int>())}");
    WriteLine();

    void print(Process employee, int level)
    {
        Write(employee.Id);
        Write(" -> ");
        if (employee.Parent != null)
        {
            print(employee.Parent, level + 1);
        }
    }
}

Read();
